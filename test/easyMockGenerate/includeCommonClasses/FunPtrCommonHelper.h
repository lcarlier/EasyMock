#ifndef FUNPTRCOMMONHELPER_H
#define FUNPTRCOMMONHELPER_H

class FunctionDeclaration;

FunctionDeclaration* getFunPtrDeclaration(unsigned int n, const char* functionName);

#endif /* FUNPTRCOMMONHELPER_H */

