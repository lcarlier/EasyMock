#ifndef VOIDSTRUCTANONTYPEDEF_H
#define VOIDSTRUCTANONTYPEDEF_H

#ifdef __cplusplus
extern "C"
{
#endif

  typedef struct
  {
    int a;
  } TypedDefAnonymousStruct;

  TypedDefAnonymousStruct structAnonymousTypedDefFunStructAnonymousTypedDef(TypedDefAnonymousStruct s1);

#ifdef __cplusplus
}
#endif

#endif /* VOIDSTRUCTANONTYPEDEF_H */

