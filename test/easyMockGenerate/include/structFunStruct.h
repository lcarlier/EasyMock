#ifndef STRUCTFUNSTRUCT_H
#define STRUCTFUNSTRUCT_H

#ifdef __cplusplus
extern "C"
{
#endif

#include "structCommonDeclaration.h"

  struct s1 structFunStruct(struct s2 s);


#ifdef __cplusplus
}
#endif

#endif /* STRUCTFUNSTRUCT_H */

