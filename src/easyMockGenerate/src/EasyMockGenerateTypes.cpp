#include "EasyMockGenerateTypes.h"

const char *easyMock_arrayCTypeStr[] =
{
  "char",
  "unsigned char",
  "short",
  "unsigned short",
  "int",
  "unsigned int",
  "long",
  "unsigned long",
  "long long",
  "unsigned long long",
  "float",
  "double",
  "long double",
  "void",
  "invalid"
};

const char *easyMock_printfFormat[] =
{
  "c",
  "c",
  "hi",
  "hu",
  "d",
  "u",
  "li",
  "lu",
  "lli",
  "llu",
  "f",
  "lf",
  "Lf",
  "voidInvalidPrintfFormat",
  "invalidPrintfFormat"
};
